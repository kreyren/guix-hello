(define-module (hello packages hello)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses)
                #:select (gpl3+)))

(define-public hello
  (package
    (name "hello")
    (version "2.12.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/hello/hello-" version
                                  ".tar.gz"))
              (sha256
               (base32
                "086vqwk2wl8zfs47sq2xpjc9k066ilmb8z6dn0q6ymwjzlm196cd"))))
    (build-system gnu-build-system)
    (synopsis "Hello, GNU world: An example GNU package")
    (description "Guess what GNU Hello prints!")
    (home-page "http://www.gnu.org/software/hello/")
    (license gpl3+)))
